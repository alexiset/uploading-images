function CreatureImage(options) {
    let hostId;

    this.count = Number(options.count);
    this.sessionId = options.sessionId;

    hostId = Math.floor(Math.random() * 100 % options.hostCount) + 1;
    this.imgUrl = 'http://images-static-' + hostId + '/img/img.jpg';

    this.init();
}

CreatureImage.prototype = {

    init: function () {
        let promisesImages = [];

        for (let i = 1; i <= this.count; i++) {
            promisesImages.push(new Promise((resolveImg, rejectImg) => {
                let img =  new Image(),
                    startTime = Date.now();

                img.src = this.imgUrl + '?' + Math.floor(Math.random() * 100000000);

                img.onload = () => resolveImg(Date.now() - startTime);
                img.onerror = rejectImg;
            }));
        }

        Promise.all(promisesImages)
            .then((timings) => {
                let maxResponseEnd = Math.max.apply(Math, timings);

                parent.postMessage(JSON.stringify({
                    sessionId: this.sessionId,
                    count: this.count,
                    time: maxResponseEnd
                }), '*');
            })
            .catch((err) => console.error('Error loading images: ', err));
    }
}

window.addEventListener('message', function (response) {
    let data;

    try {
        data = JSON.parse(response.data);
    } catch (err) {
        console.error('failed parse parameters in worker');
    }

    if (data) {
        new CreatureImage(data);
    }
});