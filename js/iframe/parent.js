;(function () {
    'use strict';

    function AddImages(options) {
        this.options = Object.assign({
            maxImagesInIteration: 50,
            hostCount: 1
        }, options);

        this.timeList = [];
        this.countImagesInIteration = 1;
        this.currentIframe = null;
        this.sessionId = Math.random().toString(32).slice(2);

        this.init();
    }

    AddImages.prototype = {

        init: function () {

            this.onWindowMessageHandler = this.responseProcessingIframe.bind(this);
            window.addEventListener('message', this.onWindowMessageHandler);

            this.next();
        },

        next: function () {
            let promiseImages;

            this.currentIframe = document.createElement('iframe');
            this.currentIframe.setAttribute('src', 'iframe.html');
            this.currentIframe.setAttribute('class', 'b-iframe');

            document.body.appendChild(this.currentIframe);

            this.currentIframe.addEventListener('load', () => {
                this.currentIframe.contentWindow.postMessage(JSON.stringify({
                    sessionId: this.sessionId,
                    count: this.countImagesInIteration,
                    hostCount: this.options.hostCount
                }), '*');
            });
        },

        responseProcessingIframe: function (response) {
            var resultFromIframe = JSON.parse(response.data);

            if (this.sessionId === resultFromIframe.sessionId) {
                this.timeList.push(resultFromIframe);

                if (this.currentIframe) {
                    document.body.removeChild(this.currentIframe);
                    delete this.currentIframe;
                }

                if ((this.countImagesInIteration + 1) <= this.options.maxImagesInIteration) {

                    this.options.progressBar.updateState(this.countImagesInIteration + 1, this.options.maxImagesInIteration);

                    this.countImagesInIteration++;
                    this.next();
                } else {
                    if (this.onWindowMessageHandler) {
                        window.removeEventListener('message', this.onWindowMessageHandler);
                        delete this.onWindowMessageHandler;
                    }

                    if (typeof this.options.done === 'function') {
                        this.options.done(this.timeList);
                    }
                }
            }
        },

        getTimeList: function() {
            return this.timeList;
        },

        getHostCount: function() {
            return this.options.hostCount;
        }
    }

    function Queue(options) {
        this.options = Object.assign({}, options);
        this.items = [];

        this.progressBarAll = new ProgressBarAll();
    }

    Queue.prototype = {

        push: function (task) {
            this.items.push(task);
        },

        run: function () {
            let task;

            if (this.items.length) {
                task = this.items.shift();
                task((data, index) => {
                    if (typeof this.options.done === 'function') {
                        this.options.done(data, index);
                    }

                    this.progressBarAll.updateState(index, this.options.totalCountHosts);

                    this.run();
                });
            }
        }
    }

    function Chart(options) {
        let chart = document.createElement('div'),
            chartId;

        this.options = Object.assign({}, options);

        chartId = 'chart_' + this.options.index;

        console.log(this.options.times);

        chart.setAttribute('id', chartId);
        document.body.appendChild(chart);

            Highcharts.chart(chartId, {
                chart: {
                    type: 'spline'
                },
                plotOptions: {
                    spline: {
                        lineWidth: 2,
                        marker: {
                            enabled: false
                        }
                    }
                },
                title: {
                    text: 'График загрузок'
                },
                yAxis: {
                    title: {
                    text: 'Затраченное время(мс)'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Количество изображений'
                    }
                },
                series: [{
                    name: 'Host Count: ' + this.options.hostCount,
                    data: this.options.times.map((point) => {
                        return {
                            x: point.count,
                            y: point.time
                        };
                    })
                }]
            });
    }

    function ProgressBar() {}

    ProgressBar.prototype = {
        init: function () {
            this.cacheDomElements();
            this.clearing();
        },

        cacheDomElements: function () {
            this.$progressBar = document.getElementById(this.progressBarClassName);
            this.$progress = document.getElementById(this.progressClassName);
        },

        clearing: function () {
            this.$progress.style.width = this.options.defaultProgressWidth;
        },

        updateState: function (countDownloaded, count) {
            let widthProgressBar = this.$progressBar.clientWidth,
                widthProgress = (widthProgressBar / count) * countDownloaded;

            this.$progress.style.width = widthProgress;
        }
    }

    function extend(Child, Parent) {
        var F = function() { }
        F.prototype = Parent.prototype
        Child.prototype = new F()
        Child.prototype.constructor = Child
        Child.superclass = Parent.prototype
    }

    function ProgressBarCurrent(options) {
        this.options = Object.assign({
            defaultProgressWidth: 0
        }, options);

        this.progressBarClassName = 'js-progress-bar-current';
        this.progressClassName = 'js-progress-bar-current__progress';

        this.init();
    }

    extend(ProgressBarCurrent, ProgressBar);

    function ProgressBarAll(options) {
        this.options = Object.assign({
            defaultProgressWidth: 0
        }, options);

        this.progressBarClassName = 'js-progress-bar-all';
        this.progressClassName = 'js-progress-bar-all__progress';

        this.init();
    }

    extend(ProgressBarAll, ProgressBar);

    window.onload = function() {
        let totalCountHosts = 10,
            queue = new Queue({
                done: (item, index) => {
                    new Chart({
                        index: index,
                        hostCount: item.getHostCount(),
                        times: item.getTimeList()
                    });
                },
                totalCountHosts: totalCountHosts
            });

        for (let i = 1; i <= totalCountHosts; i ++) {
            queue.push((callback) => {
                let item = new AddImages({
                        hostCount: i,
                        done: () => callback(item, i),
                        progressBar: new ProgressBarCurrent(),
                    });
            });
        }

        queue.run();
    };

}());