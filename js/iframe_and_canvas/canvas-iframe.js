;(function () {
	'use strict';

	function GetImages(options) {
		this.options = Object.assign({}, options);

		this.url = '/img/img.jpg'
		this.countImages = 5;
		this.promisesList = [];
		this.createImages();

		this.timeStart = Date.now();
	}

	GetImages.prototype = {
		creatImage: function (element) {
			var createdPicture = new Image();
			createdPicture.src = this.url;


			this.promisesList.push(
				new Promise((resolve, reject) => {
					createdPicture.onload = function() {
						element.context.drawImage(createdPicture, 0, 0);
						resolve(element.canvas.toDataURL());
					}.bind(this);
				})
			);
		},
		createCanvas: function () {
			var canvas = document.createElement('canvas'),
				contextCanvas;

				document.body.appendChild(canvas);

				contextCanvas = canvas.getContext('2d');
				canvas.setAttribute('class', 'b-created-canvas');

			return {
				context: contextCanvas,
				canvas: canvas
			};
		},
		createImages: function () {
			for (var i = 0; i < this.countImages; i++) {
				this.creatImage(this.createCanvas());
			};

			Promise.all(this.promisesList).then(results => {
				var aboutImages = {
						images: results,
						id: this.options.id,
						time: ((Date.now() - this.timeStart) / 1000)
					};

				parent.postMessage(JSON.stringify(aboutImages), '*');
			});
		},

	};

	document.addEventListener("DOMContentLoaded", function () {
		window.addEventListener('message', function (response) {
			var id = JSON.parse(response.data);

			new GetImages({id: id});
		});
	});

})();