(function () {
	function CreateLoaderImages(id) {
		this.countIframes = 10;
		this.idList = [];
		this.createIframes();
		this.iframes = [];
		this.images = [];

		this.id = id;
		this.count = 0;

		this.timeStart = Date.now();

		window.addEventListener('message', this.getTimeLoad);
	}

	CreateLoaderImages.prototype = {
		createIframe: function () {
			var iframe = document.createElement('iframe'),
				wrapper = document.querySelector('.iframe'),
				id = (Math.random() * 1000000).toFixed(),
				urlIframe = 'http://sub' + this.getRandomNumber() + '.images/iframe-for-canvas.html';

			iframe.setAttribute('src', urlIframe);
			iframe.setAttribute('class', 'b-iframe-cavas');
			wrapper.appendChild(iframe);

			iframe.addEventListener('load', () => {
				iframe.contentWindow.postMessage(String(id), '*');
			});

			window.addEventListener('message', this.answerIframe.bind(this));
		},
		answerIframe: function (response) {
			var answer = JSON.parse(response.data);
			if (this.idList.indexOf(answer.id) === -1) {
				this.idList.push(answer.id);

				this.images.push(answer);
			}

			if (this.idList.length === this.countIframes && this.count === 0) {
				this.count++;
				console.log((Date.now() - this.timeStart) / 1000);
				console.log(this.images);
			}

		},
		createIframes: function () {
			for (var i = 0; i < this.countIframes; i++) {
				this.createIframe();
			}
		},
		getRandomNumber: function () {
			return Math.round(1 + Math.random() * 9);;
		}
	}

	document.addEventListener("DOMContentLoaded", function () {
		new CreateLoaderImages();
	});
})();