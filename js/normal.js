;(function () {
	'use strict';

	function Normal() {
		this.init();
	}

	Normal.prototype = {
		init: function () {
			var $wrap = document.querySelector('.b-add-images'); 

			this.listImages = [];

			this.timeStart = Date.now();

			for (var i = 0; i < 50; i++) {
				this.listImages.push(

					new Promise((resolve, reject) => {
						let createdPicture;
						createdPicture = new Image();
						createdPicture.src = '/img/img.jpg';

						createdPicture.onload = function() {
							$wrap.appendChild(createdPicture);
							resolve();
						}.bind(this);
					})
				);
			}

			Promise.all(this.listImages).then(results => {
				console.log((Date.now() - this.timeStart));
			});
		}
	}
	document.addEventListener("DOMContentLoaded", function () {
		new Normal();
	});
})();