;(function () {
    'use strict';

    function AddImages() {
        this.timeList = [];
        this.imgUrl = 'img/img.jpg';
        this.queue = [];

        this.init();
    }

    AddImages.prototype = {

        init: function () {
            for (let i = 1; i <= 50; i++) {
                this.queue.push(this.getIteration(i));
            }

            this.next();
        },

        getIteration: function (count) {
            return () => {
                return new Promise((resolve, reject) => {
                    let promisesImages = [];

                    for (let i = 0; i < count; i++) {
                        promisesImages.push(new Promise((resolveImg, rejectImg) => {
                            let img =  new Image(),
                                startTime = Date.now();

                            img.src = this.imgUrl + '?' + Math.floor(Math.random() * 100000000);

                            img.onload = () => {
                                resolveImg(Date.now() - startTime);
                            };
                            img.onerror = rejectImg;
                        }));
                    }

                    Promise.all(promisesImages)
                        .then((timings) => {
                            let maxResponseEnd = Math.max.apply(Math, timings);

                            this.timeList.push({
                                count: count,
                                time: maxResponseEnd
                            });

                            console.log('image count: ', count);
                            console.log('max response time: ', maxResponseEnd);
                            resolve();
                        })
                        .catch((err) => {
                            console.error('Error loading images: ', err);
                            reject(err);
                        });
                });
            }
        },

        next: function () {
            let iteration = this.queue.shift();

            if (iteration) {
                iteration()
                    .then(() => this.next())
                    .catch((err) => console.error('iteration failed: ', err));
            }
        }
    }

    new AddImages();

}());