;(function () {
    'use strict';

    function AddImages() {
        this.timeList = [];
        this.queue = [];

        //this.init();
        this.test();
    }

    AddImages.prototype = {

        init: function () {
            var promisesImages = [];

            for (let i = 1; i <= 50; i++) {
                let promiseImages;
                (function (i) {
                    promiseImages = new Promise((resolve, reject) => {
                        var worker = new Worker('/js/worker.js');

                        worker.addEventListener('message', function(e) {
                         console.log('Worker said: ', e.data);
                         resolve(e.data);
                        }, false);

                        worker.postMessage(i);
                    });
                })(i);
                promisesImages.push(promiseImages);
            }

            Promise.all(promisesImages).then(results => {
                console.log(results);
            });
        },
        test: function () {
            var worker = new Worker('/js/worker.js');

            worker.addEventListener('message', function(e) {
             console.log('Worker said: ', e.data);
            }, false);

            worker.postMessage(10);
        }
    }

    new AddImages();

}());