function CreatureImage(count, self) {
    this.count = Number(count);
    this.that = self

    this.imgUrl = 'img/img.jpg';

    this.init();
}

CreatureImage.prototype = {
    init: function () {
        let promisesImages = [];

        for (let i = 1; i <= this.count; i++) {
            promisesImages.push(new Promise((resolveImg, rejectImg) => {
                let img =  new Image(),
                    startTime = Date.now();

                img.src = this.imgUrl + '?' + Math.floor(Math.random() * 100000000);

                img.onload = () => {
                    resolveImg(Date.now() - startTime);
                };
                img.onerror = rejectImg;
            }));
        }

        Promise.all(promisesImages)
            .then((timings) => {
                let maxResponseEnd = Math.max.apply(Math, timings),
                    result;

                this.timeList.push({
                    count: count,
                    time: maxResponseEnd
                });

                result = JSON.stringify(
                            {
                                count: count,
                                time: maxResponseEnd
                            }
                        );

                this.that.postMessage(count);

                //console.log('image count: ', count);
                //console.log('max response time: ', maxResponseEnd);
                resolve();
            })
            .catch((err) => {
                //console.error('Error loading images: ', err);
                //reject(err);
            });
    }
}


self.addEventListener('message', function(e) {
    new CreatureImage(e.data, self);
}, false);